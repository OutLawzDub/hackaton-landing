$(window).scroll(function() {
    checkScrollTop();
});

$(document).ready(function() {
    checkScrollTop();
});

function checkScrollTop()
{
    var height      = $(window).scrollTop();
    var timeAction  = 200;

    if(height > 50) {
        $('nav').stop().animate({
            backgroundColor: 'rgb(50, 50, 50)',
            height: '50px'
        }, timeAction);

        //c59114

        $('nav > ul > li.connect').animate({
            color: '#c59114'
        }, timeAction);

        $('.items').show();

        $('nav > ul > li').stop().animate({
            height: '50px',
            lineHeight: '50px'
        }, timeAction);
    } else {
        $('nav').stop().animate({
            backgroundColor: 'rgba(0, 0, 0, 0)',
            height: '100px'
        }, timeAction);

        $('nav > ul > li.connect').animate({
            color: 'rgba(50, 50, 50, 1)'
        }, timeAction);

        $('.items').hide();

        $('nav > ul > li').stop().animate({
            height: '100px',
            lineHeight: '100px'
        }, timeAction);
    }
}

$('ul.navbar > li').click(function() {
    let to          = $('.' + $(this).attr('to'));
    let timeAction  = 1000;

    $('html, body').animate({
        scrollTop: ($(to).offset().top - 50)
    }, timeAction);
});

particlesJS.load('particles-js', 'assets/json/particlesjs-config.json', function() {
    console.log('callback - particles.js config loaded');
});

$('.items')
    .on("mouseenter", function() {
        $('li[to="toggle"]').show();
    })
    .on("mouseleave", function() {
        $('li[to="toggle"]').hide();
    });

$('.items > li.top').click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
});

$('.propos > ul > li')
.on('mouseenter', function() {
    let id = $(this).attr('id');

    console.log(id);

    $('div.date[id="' + id + '"]')
        .removeClass('animated bounceOutRight')
        .addClass('animated bounceInRight')
        .show();

    $('div.content[id="' + id + '"]')
        .removeClass('animated bounceOutLeft')
        .addClass('animated bounceInLeft')
        .show();
})
.on('mouseleave', function() {
    let id = $(this).attr('id');

    $('div.date[id="' + id + '"]')
        .removeClass('animated bounceInRight')
        .addClass('animated bounceOutRight');

    $('div.content[id="' + id + '"]')
        .removeClass('animated bounceInLeft')
        .addClass('animated bounceOutLeft')
});

const slogan = [
    {
        'writer': 'J. Doe',
        'quote': 'Des évolutions en termes de compétences.',
        'year': 2019
    },
    {
        'writer': 'J. Doe',
        'quote': 'De réel projet en collaboration',
        'year': 2017
    },
    {
        'writer': 'Agent du Web',
        'quote': 'Think & Create',
        'year': 2020
    }
];

let intSlogan = 0;

function updateSlogan()  {
    let info = slogan[intSlogan];

    $('.services > p')
        .fadeOut(500);

    setTimeout(function() {
        $('.services > p.quote')
            .html('“ ' + info.quote + ' ”');
        $('.services > p.sp').html(info.writer + ' - ' + info.year);
    }, 500);

    setTimeout(function() {
        $('.services > p')
            .fadeIn(500);
    }, 1500);

    if(intSlogan == (slogan.length - 1))
    {
        intSlogan = 0;
    }
    else
    {
        intSlogan++;
    }
}

setInterval(function() {
    updateSlogan();
}, 10000);


