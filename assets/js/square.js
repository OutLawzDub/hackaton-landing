// fix me!  Make me randomly return 1 of 4 colors
function pickColor(){
    var colorArray = ["rgb(183, 127, 21)", "rgb(211, 153, 25)", "rgb(210, 163, 31)", "rgb(209, 177, 40)"];
    return colorArray[Math.floor(Math.random() * colorArray.length)];
}

// fix me!  Make me randomly return a number.  You can pick the range of numbers returned.
function pickPos(){
    return Math.floor((Math.random() * window.innerWidth) + 10) + "px";
}

function addSquare(){
    var div = document.createElement("div");

    div.style.background = pickColor();
    div.className = 'squarand';

    div.style.left=pickPos();
    div.style.top=pickPos();

    $(".propos").append(div);
}

for (let i = 0; i < 20; i++)
{
    addSquare();
}
