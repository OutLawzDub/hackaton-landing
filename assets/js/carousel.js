let nbLi    = $('ul.content > li').length;
let actual  = 0;

$('.arrow').click(function() {
    if($(this).hasClass('left'))
    {
        if(actual >= 0)
        {
            let comeBack = (actual - 1);

            $('li[id="' + comeBack + '"]')
                .removeClass('animated zoomOutLeft')
                .addClass('animated zoomInLeft')
                .show();

            if(actual != 0)
            {
                actual = actual - 1;
            }
        }
    }

    if($(this).hasClass('right'))
    {
        if(actual <= (nbLi - 2))
        {
            let toHide = actual;

            $('li[id="' + actual + '"]')
                .addClass('animated zoomOutLeft');


            setTimeout(function() {
                $('li[id="' + toHide + '"]')
                    .hide();
            }, 700);

            actual++;
        }
    }
});

$(document).keydown(function(e) {
    switch(e.which) {
        case 37: // left
            break;

        case 39: // right
            break;

        default: return; // exit this handler for other keys
    }
    e.preventDefault(); // prevent the default action (scroll / move caret)
});